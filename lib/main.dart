import 'package:flutter/material.dart';
import 'file:///C:/Users/baay4/AndroidStudioProjects/login-signup/lib/utils/constants.dart';
import 'package:flutter_auth/ui/Screens/Login/login_screen.dart';



void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Flutter Login',
      theme: ThemeData(
        primaryColor: kPrimaryColor,
        scaffoldBackgroundColor: Colors.white,
      ),
      home: LoginScreen(),
    );
  }
}
