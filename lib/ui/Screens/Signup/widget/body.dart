import 'package:flutter/material.dart';
import 'package:flutter_auth/ui/Screens/Login/login_screen.dart';
import 'package:flutter_auth/ui/Screens/Signup/widget/or_divider.dart';
import 'package:flutter_auth/ui/Screens/Signup/widget/social_icon.dart';
import 'package:flutter_auth/ui/widget_components/already_have_an_account_acheck.dart';
import 'package:flutter_auth/ui/widget_components/rounded_button.dart';
import 'package:flutter_auth/ui/widget_components/rounded_input_field.dart';
import 'package:flutter_auth/ui/widget_components/rounded_password_field.dart';
import 'package:flutter_svg/svg.dart';
import 'package:lottie/lottie.dart';

class Body extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Container(
      child: Center(
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              SizedBox(height: size.height * 0.03),
              Lottie.network(
                "https://assets3.lottiefiles.com/packages/lf20_u8o7BL.json",
                height: size.height * 0.35,
              ),
              RoundedInputField(
                hintText: "Email",
                onChanged: (value) {},
              ),
              RoundedPasswordField(
                onChanged: (value) {},
              ),
              RoundedButton(
                text: "SIGNUP",
                press: () {},
              ),
              SizedBox(height: size.height * 0.03),
              AlreadyHaveAnAccountCheck(
                login: false,
                press: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) {
                        return LoginScreen();
                      },
                    ),
                  );
                },
              ),
              OrDivider(),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  SocalIcon(
                    iconSrc: "assets/icons/facebook.svg",
                    press: () {},
                  ),
                  SocalIcon(
                    iconSrc: "assets/icons/twitter.svg",
                    press: () {},
                  ),
                  SocalIcon(
                    iconSrc: "assets/icons/google-plus.svg",
                    press: () {},
                  ),
                ],
              )
            ],
          ),
        ),
      ),
    );
  }
}
