import 'package:flutter/material.dart';
import 'package:flutter_auth/ui/Screens/Signup/signup_screen.dart';
import 'package:flutter_auth/ui/widget_components/already_have_an_account_acheck.dart';
import 'package:flutter_auth/ui/widget_components/rounded_button.dart';
import 'package:flutter_auth/ui/widget_components/rounded_input_field.dart';
import 'package:flutter_auth/ui/widget_components/rounded_password_field.dart';
import 'package:flutter_svg/svg.dart';
import 'package:lottie/lottie.dart';

class Body extends StatelessWidget {
  const Body({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Container(
      child: Center(
        child: SingleChildScrollView(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              SizedBox(height: size.height * 0.03),
              Lottie.network(
                "https://assets3.lottiefiles.com/packages/lf20_Udhqdd.json",
                height: size.height * 0.35,
              ),
              SizedBox(height: size.height * 0.03),
              RoundedInputField(
                hintText: "Email",
                onChanged: (value) {},
              ),
              RoundedPasswordField(
                onChanged: (value) {},
              ),
              RoundedButton(
                text: "LOGIN",
                press: () {},
              ),
              SizedBox(height: size.height * 0.03),
              AlreadyHaveAnAccountCheck(
                press: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) {
                        return SignUpScreen();
                      },
                    ),
                  );
                },
              ),
            ],
          ),
        ),
      ),
    );
  }
}
